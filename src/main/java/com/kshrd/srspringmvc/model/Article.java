package com.kshrd.srspringmvc.model;


import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Article {
    private int id;

    //@NotEmpty(message = "Title cannot be empty..")
    private String title;

    //@NotEmpty
    private String description;

    private String images;

    //@NotEmpty
    private String actions;
}
