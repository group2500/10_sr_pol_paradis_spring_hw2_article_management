package com.kshrd.srspringmvc.repository;

import com.github.javafaker.Faker;
import com.kshrd.srspringmvc.model.Article;
import jdk.jfr.Description;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ArticleRepository {

    List<Article> articles = new ArrayList<>();
   public ArticleRepository(){
        // add some value to the articles (list )

        //for (int i =0 ; i<3 ; i++){
       /*
            Article article = new Article();
            Faker faker = new Faker();
            article.setId(i+1);
            article.setTitle(faker.name().fullName());
            article.setDescription(faker.name().fullName());
            article.setImages("http://localhost:8282/images/");
            article.setActions("View");
            articles.add(article);*/
        //}

       Article article = new Article();
       article.setId(1);
       article.setTitle("Spring");
       article.setDescription("Spring makes programming Java quicker, easier, and safer for everybody. Spring’s focus on speed, simplicity, and productivity has made it the world’s most popular Java Framework.");
       article.setImages("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuVeXzGVYf_O8C1tSNC2eDLkxE41NKenqRTzN7cGOpfhdxhEz95lOFMEX8vPwJuxJx1Uc&usqp=CAU");
       article.setActions("View");
       articles.add(article);

       Article article2 = new Article();
       article2.setId(2);
       article2.setTitle("Docker");
       article2.setDescription("Docker takes away repetitive, mundane configuration tasks and is used throughout the development lifecycle for fast, easy and portable application development desktop and cloud. Docker’s comprehensive end to end platform includes UIs, CLIs, APIs, and security that are engineered to work together across the entire application delivery lifecycle. ");
       article2.setImages("https://camo.githubusercontent.com/a9dfb051e5a719377e6633fff5ff46e24e57dbe038993900ade89dcf0b54b337/68747470733a2f2f63646e2e316d696e33302e636f6d2f77702d636f6e74656e742f75706c6f6164732f323031382f30352f4c6f676f2d446f636b65722d312e6a7067");
       article2.setActions("View");
       articles.add(article2);

       Article article3 = new Article();
       article3.setId(3);
       article3.setTitle("Linux");
       article3.setDescription("Linux is a family of open-source Unix-like operation system based on the Linux kernel, an operation system kernel first released on September 17, 1991, by Linus Torvalds. Linus is packages in a Linux distribution. ");
       article3.setImages("https://d1yjjnpx0p53s8.cloudfront.net/styles/logo-thumbnail/s3/0023/4014/brand.gif?itok=M_d2kGqH");
       article3.setActions("View");
       articles.add(article3);

    }
  public   List<Article> getAllArticle(){
        return articles;
   }

    public void addNewArticle(Article article){
       // 0
       article.setId(articles.size()+1);
       articles.add(article);
    }

    public void updateArticle(Article article){
        articles.set(article.getId() - 1, article);
    }

    public void deleteArticle(Article article){
        articles.remove(article);
    }

    public Article findArticleByID(int id ){
       return articles.stream().filter(article -> article.getId()== id).findFirst().orElseThrow();
    }
}
