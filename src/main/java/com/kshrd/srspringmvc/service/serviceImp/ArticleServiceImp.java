package com.kshrd.srspringmvc.service.serviceImp;

import com.kshrd.srspringmvc.model.Article;
import com.kshrd.srspringmvc.repository.ArticleRepository;
import com.kshrd.srspringmvc.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    @Override
    public List<Article> getAllArticles() {
        return articleRepository.getAllArticle();
    }

    @Override
    public Article findArticleByID(int id) {
        return articleRepository.findArticleByID(id);
    }

    @Override
    public void articleMethod() {

    }

    @Override
    public void AddArticleMethod(Article article) {
        articleRepository.addNewArticle(article);
    }

    @Override
    public void UpdateArticleMethod(Article article) {
        articleRepository.updateArticle(article);
    }

    @Override
    public void DeleteArticleMethod(Article article) {
        articleRepository.deleteArticle(article);
    }
}
