package com.kshrd.srspringmvc.service;


import com.kshrd.srspringmvc.model.Article;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {

    public List<Article> getAllArticles();
    public Article findArticleByID(int id );
    public void articleMethod();
    public void AddArticleMethod(Article article);

    public void UpdateArticleMethod(Article article);

    public void DeleteArticleMethod(Article article);
}
