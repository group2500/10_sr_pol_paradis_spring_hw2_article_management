package com.kshrd.srspringmvc.controller;

import com.kshrd.srspringmvc.model.Article;
import com.kshrd.srspringmvc.service.ArticleService;
import com.kshrd.srspringmvc.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@Controller
public class ArticleController {

    @Autowired
  @Qualifier("articleServiceImp")

    ArticleService articleService;
    // 3 methods : field, setter, constructor

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles", articleService.getAllArticles());
        return "index";
    }


    @GetMapping("/form-add")
    public String showFormAdd(Model model){

        model.addAttribute("Article",new Article());
        return "form-add";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Article article, @RequestParam("file") MultipartFile file, BindingResult bindingResult){

        try{
            String filename = fileStorageService.saveFile(file);
            System.out.println("filename: " +filename);

        }catch (IOException ex){
            System.out.println("Error with the Image upload "+ex.getMessage());
        }


        if (bindingResult.hasErrors()){
            return "/form-add";
        }

        articleService.AddArticleMethod(article);
        return  "redirect:/";
    }


    @GetMapping("/view-article/{id}")
    public String viewArticle(@PathVariable int id,Model model){
        // Find Article By ID
        Article resultArticle = articleService.findArticleByID(id);

        // send value to the view
        model.addAttribute("article", resultArticle);

        return "form-view";
    }

    @GetMapping("/update-article/{id}")
    public String updateArticle(@PathVariable int id,Model model){

        Article resultArticle = articleService.findArticleByID(id);

        // send value to the view
        model.addAttribute("article", resultArticle);

        return "form-update";
    }

    @PostMapping("/handle-update")
    public String handleUpdate(@ModelAttribute @Valid Article article, @RequestParam("file") MultipartFile file, BindingResult bindingResult){

        try{
            String filename = fileStorageService.saveFile(file);
            System.out.println("filename: " +filename);

        }catch (IOException ex){
            System.out.println("Error with the Image upload "+ex.getMessage());
        }


        if (bindingResult.hasErrors()){
            return "/form-update";
        }

        articleService.UpdateArticleMethod(article);
        return  "redirect:/";
    }

    @GetMapping("/delete-article/{id}")
    public String deleteArticle(@PathVariable int id,Model model){

        Article article = articleService.findArticleByID(id);
        articleService.DeleteArticleMethod(article);
        return  "redirect:/";
    }
}
